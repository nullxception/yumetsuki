package io.chaldeaprjkt.yumetsuki.constant

object Source {
    const val Salt = "6cqshh5dhw73bzxn20oexa9k516chk7s"
    const val HoYoLAB = "https://www.hoyolab.com"
    const val BBSAPI = "https://bbs-api-os.hoyoverse.com"

    object App {
        const val Git = "https://gitlab.com/nullxception/yumetsuki"
        const val Disclaimer = "$Git/blob/main/disclaimer.md#disclaimer"
        const val PrivacyPolicy = "$Git/blob/main/disclaimer.md#privacy-policy"
    }
}
